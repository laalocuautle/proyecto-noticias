<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Visita;
class Noticia extends Model{
    //
    protected $guarded = [];

    protected function findBySlug($slug){
        return $this->where('slug','=',$slug)->first();
    }

    protected function visitas($id){
        return Visita::where('noticia_id',$id)->count();
    }

    public function visita(){
        return $this->hasMany(Visita::class);
    }
}
