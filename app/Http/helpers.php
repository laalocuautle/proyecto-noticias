<?php
use Carbon\Carbon;
function fromNow($fecha){
    $ts = Carbon::createFromFormat('Y-m-d H:i:s', $fecha);
    return $ts->diffForHumans();
}