<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use stdClass;
use App\Noticia;
use Carbon\Carbon;
class SpidersController extends Controller{

    public static function Peru(){
        $client = new Client();
        $crawler = $client->request('GET','https://peru.com/epic/tecnologia');

        $crawler->filter('div > article.cnt_flujo')->each(function ($node) {
            $client = new Client();
            $data = new Noticia();
            $link = $node->filter('h2 > a')->attr('href');
            $data->ref = $link;
            $crawlerDe = $client->request('GET', $link);
            $titulo = $crawlerDe->filter('article.nota > h1')->text();
            $result = Noticia::where('titulo','=', $titulo)->count();
            if ($result === 0){
                $data->fecha_original = Carbon::parse($crawlerDe->filter('li.fecha > a > time')->attr('datetime'));
                $data->titulo = $titulo;
                $data->imagen = $crawlerDe->filter('article.nota > div > figure > div > img')->attr('src');
                $data->descripcion = $crawlerDe->filter('div#contenedor > p')->each(function ($node) {

                    return $node->text();
                });
                $data->descripcion = json_encode($data->descripcion);
                $data->sitio = 'peru';
                $data->categoria = 'tecnologia';
                $data->slug = str_slug($data->titulo, '-');
                $data->save();
            }
        });
    }

}
