<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noticia;
use Chencha\Share\ShareFacade as Share;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\SpidersController as Spider;
use Illuminate\Support\Facades\Storage;
use App\Visita;
class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        //$tw = Share::load('http://www.example.com', 'My example')->twitter();
        //return redirect()->to($tw)->send();

        if( isset($_GET['s']) )
            switch ($_GET['s']){
                case 'peru' :
                    Spider::Peru();

                default :
                    return redirect('/');
            }
        $noticias = Noticia::where('estado','=',1)
            ->where('activo',true)
            ->orderBy('fecha_original','desc')
            ->paginate(10);
        return view('welcome', compact('noticias'));
    }
    public function show($categoria,$slug){

        $key = md5($slug);
        if(!Storage::disk('local')->exists($key)){
            $detalle = Noticia::findBySlug($slug);
            Visita::create(['noticia_id' => $detalle->id]);
            Storage::disk('local')->put($key, $detalle);
        }else{
            $data = json_decode(Storage::get($key));
            Visita::create(['noticia_id' => $data->id]);
            $detalle = $data;

        }
        return view('site.detalle', compact('detalle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
