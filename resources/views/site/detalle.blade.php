<div>
    {{$detalle->titulo}}
    <br>
    {{ App\Noticia::visitas($detalle->id)  }}
    @foreach(json_decode($detalle->descripcion) as $p )
        <p>
            {{ $p }}
        </p>
    @endforeach
    <br>
    <span>
        {{ fromNow( $detalle->fecha_original )  }}
    </span>
</div>