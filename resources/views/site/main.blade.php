<div class="container">
    <div class="row">
        @foreach($noticias as $noticia)
            <div class="col-4">
                <div class="card">
                    <!--img src="{{$noticia->imagen}}" alt="" class="card-img-top"-->
                    <div class="card-body">
                        <a href="/{{$noticia->categoria}}/{{$noticia->slug}}" class="list-unstyled">
                            {{$noticia->titulo}}
                        </a>
                        <p class="text-muted">
                            {{ substr(json_decode($noticia->descripcion)[0],0,120)  }}
                        </p>
                        <span class="text-muted">
                            {{$noticia->categoria}}
                        </span>
                        <br>
                        <span class="text-muted">
                            {{$noticia->visita->count()}}
                        </span>
                        <br>
                        <time>
                            {{ fromNow($noticia->fecha_original)  }}
                        </time>
                    </div>
                </div>

            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-6">
            {{$noticias->links('pagination::bootstrap-4')}}
        </div>
    </div>
</div>